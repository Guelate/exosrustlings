// structs1.rs
// Address all the TODOs to make the tests pass!



#[derive(Debug)]

struct ColorClassicStruct {
    // TODO: Something goes here
    couleur : String,
    couleur_de_fond : String,
}

struct ColorTupleStruct(/* TODO: Something goes here */ i32 , i32 , i32);

#[derive(Debug)]
struct UnitStruct;

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn classic_c_structs() {
        // TODO: Instantiate a classic c struct!
        // let green =
        let green = ColorClassicStruct {
            username: String::from("blanc"),
            email: String::from("bleu"),
        };

        assert_eq!(green.name, "green");
        assert_eq!(green.hex, "#00FF00");
        
    }

    #[test]
    fn tuple_structs() {
        // TODO: Instantiate a tuple struct!
        // let green =
        let green =  ColorTupleStruct(100, 50 , 115);

        assert_eq!(green.0, "green");
        assert_eq!(green.1, "#00FF00");
    }

    #[test]
    fn unit_structs() {
        // TODO: Instantiate a unit struct!
        // let unit_struct =
        let _unit = UnitStruct;
        let message = format!("{:?}s are fun!", _unit);

        assert_eq!(message, "UnitStructs are fun!");
    }
}
